import React, { useEffect } from 'react'
import { useState } from 'react'
function App() {
  const [time, setTime] = useState(0);
  const [running, setRunning] = useState(false);
  useEffect(() => {
    let interval;
    if (running) {
      interval = setInterval(() => {
        setTime((prevTime) => prevTime + 10)
      }, 10)
    } else if (!running) {
      clearInterval(interval)
    }
    return () => clearInterval(interval);
  }, [running])
  return (
    <div className='max-w-md flex flex-col justify-center items-center py-8 px-5'>
      <h1 className='text-2xl font-semibold pb-4 text-center'>01-stopWatch</h1>
      <div className='text-xl font-semibold py-4 text-center'>
        <span>{("0" + Math.floor((time / 60000) % 100)).slice(-2)}:</span>
        <span>{("0" + Math.floor((time / 1000) % 60)).slice(-2)}:</span>
        <span>{("0" + Math.floor((time / 10) % 100)).slice(-2)}</span>
      </div>
      <div className='d-flex justify-content-center'>
        {
          running ? (<button className='btn btn-info mx-4' onClick={() => setRunning(false)} >Stop</button>) : (<button className='btn btn-info mx-4' onClick={() => setRunning(true)} >Start</button>)
        }
        <button className='btn btn-success mx-4' onClick={() => setTime(0)}>Reset</button>
      </div>
    </div>
  )
}

export default App

